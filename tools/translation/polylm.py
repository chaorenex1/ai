# git clone https://github.com/modelscope/modelscope
# cd modelscope
# pip install .

from modelscope.pipelines import pipeline
from modelscope.utils.constant import Tasks
from modelscope import snapshot_download

polylm_13b_model_id = 'damo/nlp_polylm_13b_text_generation'
revision = 'v1.0.3'

model_dir = snapshot_download(polylm_13b_model_id, revision)

input_text = f"Beijing is the capital of China.\nTranslate this sentence from English to Chinese."

kwargs = {"do_sample": False, "num_beams": 4, "max_new_tokens": 128, "early_stopping": True, "eos_token_id": 2}
pipeline_ins = pipeline(Tasks.text_generation, model=model_dir)

result = pipeline_ins(input_text, **kwargs)
print(result['text'])