from transformers import MBartForConditionalGeneration, MBart50TokenizerFast

article_hi = "संयुक्त राष्ट्र के प्रमुख का कहना है कि सीरिया में कोई सैन्य समाधान नहीं है"
article_zh = "这是一部以美食为题材的纪录片，他采用了国际化的制作方式和表达手法，呈现的却是中国人最草根的生活方式和最浓郁的乡土情。为了寻找中国人餐饮文化的本源，摄制组花费了十三个月的时间，到全国八十多个地方采缝拍摄深入中国最边缘的角落，寻访美食的由来和民间的高手记录中国人最质朴的生活方式和对待食物的情感，他激发的是口水加泪水，台下舌尖又上筋头，是观众，尤其是海外华人观众对片中传递的爱国思乡之情最直接的感受。他在短短一年的时间里，行销全球四十三个国家和地区，不仅在东南亚等传统中国电视节目市场受到欢迎，更在欧美主流节目市场取得了不俗的销售业绩。美食大国、法国的多家机构购买了电视播映权，他成功的跨越了语言、社会价值观等传播障碍，为中华文化走出去，探索出了珍贵的经验和启示。";
article_ar = "الأمين العام للأمم المتحدة يقول إنه لا يوجد حل عسكري في سوريا."
article_zh1 = "这是一部以美食为题材的纪录片，他采用了国际化的制作方式和表达手法，呈现的却是中国人最草根的生活方式和最浓郁的乡土情。为了寻找中国人餐饮文化的本源，摄制组花费了十三个月的时间，到全国八十多个地方采缝拍摄深入中国最边缘的角落，寻访美食的由来和民间的高手记录中国人最质朴的生活方式和对待食物的情感，他激发的是口水加泪水，台下舌尖又上筋头，是观众，尤其是海外华人观众对片中传递的爱国思乡之情最直接的感受。他在短短一年的时间里，行销全球四十三个国家和地区，不仅在东南亚等传统中国电视节目市场受到欢迎，更在欧美主流节目市场取得了不俗的销售业绩。美食大国、法国的多家机构购买了电视播映权，他成功的跨越了语言、社会价值观等传播障碍，为中华文化走出去，探索出了珍贵的经验和启示。";
article_zh2 = "美食大国法国的多家机构购买了电视播映权，他成功的跨越了语言、社会价值观等传播障碍，为中华文化走出去，探索出了珍贵的经验和启示。";

model = MBartForConditionalGeneration.from_pretrained("facebook/mbart-large-50-many-to-many-mmt")
tokenizer = MBart50TokenizerFast.from_pretrained("facebook/mbart-large-50-many-to-many-mmt")

# translate Hindi to French
tokenizer.src_lang = "hi_IN"
encoded_hi = tokenizer(article_hi, return_tensors="pt")
generated_tokens = model.generate(
    **encoded_hi,
    forced_bos_token_id=tokenizer.lang_code_to_id["fr_XX"]
)
textList = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)
print(textList)
# => "Le chef de l 'ONU affirme qu 'il n 'y a pas de solution militaire dans la Syrie."

# translate Arabic to English
tokenizer.src_lang = "ar_AR"
encoded_ar = tokenizer(article_ar, return_tensors="pt")
generated_tokens = model.generate(
    **encoded_ar,
    forced_bos_token_id=tokenizer.lang_code_to_id["en_XX"]
)
textList2 = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)
print(textList2)
# => "The Secretary-General of the United Nations says there is no military solution in Syria."

# translate Chinese to English
tokenizer.src_lang = "zh_CN"
tokenizer.model_max_length = 2048
encoded_zh = tokenizer(article_zh2, return_tensors="pt")
# 打印encoded_zh的长度
# print(len(article_zh))
generated_tokens = model.generate(
    **encoded_zh,
    # num_beams=4,
    # max_length=2048,
    forced_bos_token_id=tokenizer.lang_code_to_id["en_XX"]
)
textList3 = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)
print(textList3)
# => "This is a documentary on food, which adopts international production methods and expression techniques, but presents the most grassroots way of life and the most rich local sentiment of the Chinese people. In order to find the origin of Chinese food culture, the production team spent thirteen months, filming in more than eighty places across the country, to find the origin of food and the folk masters to record the most simple way of life of the Chinese people and the emotions of treating food. What he inspired was saliva and tears, and the audience, especially overseas Chinese audience, felt the most direct love for the country and homesickness conveyed in the film. In just one year, he has marketed in forty-three countries and regions around the world, not only welcomed in traditional Chinese TV program markets such as Southeast Asia, but also achieved impressive sales performance in mainstream European and American TV program markets. Many institutions in the food country, France, have purchased TV broadcasting rights. He successfully crossed the barriers of language, social values, and other communication barriers, and explored precious experience and enlightenment for Chinese culture to go out."
