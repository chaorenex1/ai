from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

tokenizer = AutoTokenizer.from_pretrained("Helsinki-NLP/opus-mt-zh-en")

model = AutoModelForSeq2SeqLM.from_pretrained("Helsinki-NLP/opus-mt-zh-en")

article_zh = "这是一部以美食为题材的纪录片，它采用了国际化的制作方式和表达手法，呈现的却是中国人最草根的生活方式和最浓郁的乡土情节。为了寻找中国人餐饮文化的本源，摄制组花费了13个月的时间，到全国80多个地方采风拍摄，深入中国最边缘的角落，寻访美食的由来和民间的高手，记录中国人最质朴的生活方式和对待事物的情感。他激发的是口水加泪水，才下舌尖右上心头，是观众尤其是海外华人观众对片中传递的爱国思乡之情最直接的感受。他在短短一年的时间里，行交全球43个国家和地区，不仅在东南亚等传统中国电视节目市场受到欢迎，更在欧美主流节目市场取得了不俗的销售业绩。美食大国法国的多家机构购买了电视播映权，他成功的跨越了语言、社会价值观等传播障碍，为中华文化走出去探索出了珍贵的经验和启示。"
tokenizer.src_lang = "zh_CN"
# tokenizer.model_max_length = 2048
inputs = tokenizer(article_zh, return_tensors="pt", padding=True, truncation=True)
# encoded_zh = tokenizer(article_zh, return_tensors="pt")
# 打印encoded_zh的长度
# print(len(article_zh))
generated_tokens = model.generate(
    **inputs,
    # num_beams=4,
    # max_length=2048,
    # forced_bos_token_id=tokenizer.lang_code_to_id["en_XX"]
)
textList3 = tokenizer.batch_decode(generated_tokens, skip_special_tokens=True)
print(textList3)